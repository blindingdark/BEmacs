;;; http-sever.el --- Emacs as a HTTP server        -*- lexical-binding: t; -*-
;;; Commentary:
;;; Code:

(defvar http-server-callbacks '())

(defun http-server-entrance (process event)
  "The main entrance of the HTTP server.
See `make-network-process' `:sentinel' for PROCESS and EVENT."
  (cond
   ((string-prefix-p "open from" event)
    (dolist (callback http-server-callbacks)
      (funcall callback process event))
    (process-send-string process
                         (concat "HTTP/1.1 200 OK\r\n"
                                 "Content-Type: text/plain\r\n"
                                 "Content-Length: 4\r\n"
                                 "\r\n"
                                 "pong")))
   ((string= "connection broken by remote peer\n" event)
    (kill-buffer (process-buffer process)))))

(defun http-server-start-server ()
  "Start the HTTP server."
  (ignore-errors
    (make-network-process
     :noquery t
     :name "main-http-server"
     :server t
     :service 60096
     :sentinel #'http-server-entrance)))

(defun http-server-start-server-after-init ()
  "Start the HTTP server after init."
  (if after-init-time
      (http-server-start-server)
    (add-hook
     'after-init-hook
     #'(lambda () (http-server-start-server)))))

(provide 'http-server)
;;; http-server.el ends here
