;;; macport-emacs --- macport-emacs 专用
;;; Commentary:
;; macport-emacs meta - alt 互换

;;; Code:
;; Keybonds
(global-set-key [(hyper a)] 'mark-whole-buffer)
(global-set-key [(hyper v)] 'yank)
(global-set-key [(hyper c)] 'kill-ring-save)
(global-set-key [(hyper s)] 'save-buffer)
(global-set-key [(hyper l)] 'goto-line)
(global-set-key [(hyper w)]
                (lambda () (interactive) (delete-window)))
(global-set-key [(hyper z)] 'undo)

(setq mac-option-modifier 'meta)
(setq mac-command-modifier 'hyper)

(provide 'macport-emacs)
;;; macport-emacs.el ends here
