;; tab 替换为空格
(setq-default indent-tabs-mode nil)
;; tab 宽度
(setq-default tab-width 4)
;; 中文折行
(setq word-wrap-by-category t)

;; 取消 backward-kill-word
(global-set-key (kbd "<C-backspace>") nil)

;; 删除一行
(global-set-key (kbd "C-M-k") 'kill-whole-line)

;; 标记整行
(global-set-key (kbd "M-=") (kbd "C-a C-SPC C-e"))

;; 鼠标滚轮平滑滚动
(setq mouse-wheel-progressive-speed nil) ;; 关闭滚轮加速
(setq mouse-wheel-scroll-amount '(3 ((shift) . 1))) ;; 一次滚动3行

;; auto refresh file changes
(global-auto-revert-mode t)

;; easy to view/quit help window
(setq help-window-select t)

;; F5 重新加载文件
(defun refresh-file ()
  (interactive)
  (revert-buffer t (not (buffer-modified-p)) t)
  (message "refresh file successfully"))
;; (global-set-key [(control f5)] 'refresh-file)
(global-set-key [f5] 'refresh-file)

;; 显示完整路径
(leaf file-info
  :ensure t
  :bind (("C-c n" . file-info-show)
         ("C-c C-n" . file-info-show))
  :preface
  (defun file-info--low-line-replace (value)
    (cond
     ((stringp value)
      ;; COMBINING CONJOINING MACRON BELOW
      (subst-char-in-string ?_ ?︭ value))
     (t
      value)))
  (advice-add
   'file-info--get-pretty-information
   :filter-return
   #'file-info--low-line-replace))

;; 行号
(leaf display-line-numbers
  :hook (prog-mode-hook text-mode-hook web-mode-hook)
  :setq ((display-line-numbers-width . 2)
         (display-line-numbers-grow-only . t)))

(leaf vundo
  :vc (vundo :url "https://github.com/casouri/vundo.git")
  :setq (vundo-glyph-alist . vundo-unicode-symbols)
  :bind (("C-x u" . vundo)
         ("C-x C-u" . vundo)))

;; 选中光标所在单词
(leaf expand-region
  :ensure t
  :bind (("M-SPC" . er/expand-region)))

;; 全局大写字母分词
(global-subword-mode)

;; 键盘翻页平滑
(setq scroll-conservatively most-positive-fixnum
      scroll-margin 0
      scroll-preserve-screen-position 't)

;; 行首行尾
(leaf mwim
  :ensure t
  :bind (("C-a" . mwim-beginning)
         ("C-e" . mwim-end)))

;; markdown
(leaf markdown-mode
  :ensure t
  :pre-setq
  ((markdown-fontify-code-blocks-natively . t))
  :mode (("\\.markdown\\'" . gfm-mode)
         ("\\.md\\'" . gfm-mode)))

(leaf ob-elixir
  :vc (ob-elixir :url "https://github.com/lambdart/ob-elixir.git")
  :require t
  :after org
  :init
  (advice-add 'org-babel-elixir-start-process :before (lambda (&rest _args) (setenv "TERM" "vt100"))))

(leaf ob-erlang
  :require t
  :vc (ob-erlang :url "https://github.com/xfwduke/ob-erlang.git")
  :after (org))

(leaf ob-shell :require t :after (org))

(leaf org
  :ensure org-contrib
  :interpreter ("org")
  :pre-setq `((org-display-remote-inline-images . 'download)
              (org-startup-with-inline-images . t)
              (org-id-locations-file . ,(cache-path ".org-id-locations")))
  :config
  (setq org-src-fontify-natively t
        org-edit-src-content-indentation 0)
  (define-key org-mode-map (kbd "<C-tab>") nil)
  (define-key org-mode-map (kbd "C-c C-v") nil)
  (define-key org-mode-map (kbd "<M-SPC>") nil)
  (setq org-startup-folded 'nofold)
  (when (version< "9.1.4" (org-version))
    (add-to-list 'org-modules 'org-tempo))
  (add-to-list 'org-structure-template-alist
               (if (version< "9.1.4" (org-version))
                   '("S" . "src emacs-lisp")
                 '("S" "#+BEGIN_SRC emacs-lisp\n?\n#+END_SRC" "<src lang=\"emacs-lisp\">\n\n</src>")))
  (add-to-list 'org-src-lang-modes '("elixir" . elixir-ts))
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((sql . t)
     (erlang . t)
     (elixir . t)
     (ruby . t)
     (js . t)
     (python . t)
     (shell . t))))

(leaf so-long
  :require t
  :init
  (global-so-long-mode 1))

(provide 'edit/basic)
