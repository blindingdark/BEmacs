;;; dracula --- dracula theme
;;; Commentary:
;; 一个很漂亮的暗色系主题
;; 就是终端下有点不好看，适合 GUI
;; 混搭 doom 主题的警告闪烁提醒

;;; Code:
(leaf dracula-theme
  :ensure t
  :init
  (load-theme 'dracula t)
  (set-face-background 'mode-line-inactive "gray25")
  (set-face-foreground 'mode-line-inactive "gray50"))

(leaf doom-themes
  :ensure t
  :init
  (doom-themes-visual-bell-config))

(provide 'theme-dracula)
;;; theme-dracula.el ends here
