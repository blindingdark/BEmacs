(when *is-a-mac* (setq-default dired-use-ls-dired nil))

;; 设置垃圾回收，在 Windows 下，emacs25 版本会频繁触发垃圾回收，所以需要设置
(when *is-a-windows*
  (setq gc-cons-threshold (* 512 1024 1024))
  (setq gc-cons-percentage 0.5)
  (run-with-idle-timer 5 t 'garbage-collect))
;; 显示垃圾回收信息，这个可以作为调试用
;; (setq garbage-collection-messages t)

(set-buffer-file-coding-system 'utf-8-unix 't)
(prefer-coding-system 'utf-8-unix)

;; 不压缩字体缓存，加快 GC
(setq inhibit-compacting-font-caches t)

;; Default is 4096, increase this can speed up lsp-mode and some other modes.
(setq read-process-output-max (* 1024 1024)) ;; 1 MiB

(setq use-short-answers t)

;; XPS 触摸板双指上下滚动 FIXME 这个和鼠标侧键映射很容易混
(global-set-key (kbd "<mouse-5>") 'scroll-up-line)
(global-set-key (kbd "<mouse-4>") 'scroll-down-line)

;; mac 按键绑定
(leaf macport-emacs
  :when (string-match "Carbon" (version))
  :require t)

(setq backup-directory-alist `(("." . ,(cache-path "saves"))))

(add-hook 'after-init-hook
          #'(lambda ()
              (message "Emacs loaded in %.03fs"
                       (float-time (time-subtract (current-time) before-init-time)))))

(provide 'system/basic)
