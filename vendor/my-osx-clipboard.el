;;; my-osx-clipboard --- sync clipboard between shell and GUI
;;; Commentary:
;; Mac OS 终端下共享剪贴板

;;; Code:
(defun copy-from-osx ()
  (shell-command-to-string "pbpaste"))

(defun paste-to-osx (text &optional push)
  (let ((process-connection-type nil))
    (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
      (process-send-string proc text)
      (process-send-eof proc))))

(setq interprogram-cut-function 'paste-to-osx)
(setq interprogram-paste-function 'copy-from-osx)

(provide 'my-osx-clipboard)
;;; my-osx-clipboard.el ends here
