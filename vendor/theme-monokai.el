;;; my-monokai --- monokai theme
;;; Commentary:
;; 经典主题之一
;; 终端下的效果不错
;; 开启 powerline

;;; Code:
(leaf powerline
  :ensure t
  :require t
  :config
  (powerline-default-theme)
  (set-face-background 'powerline-active1 "gray30")
  (set-face-background 'powerline-active2 "gray21")
  (set-face-background 'powerline-inactive2 "gray14")
  (when (powerline-selected-window-active)
    (set-face-background 'mode-line-buffer-id-inactive "gray25")))

(leaf monokai-theme
  :ensure t
  :config
  (load-theme 'monokai t))

;; mode-line
(set-face-background 'mode-line "gray44")
(set-face-background 'mode-line-inactive "gray20")
(set-face-background 'mode-line-buffer-id "gray51")

;; vertical-border
(set-face-background 'vertical-border "gray30")

;; menu
(set-face-background 'menu "gray44")
(set-face-background 'tty-menu-enabled-face "gray20")
(set-face-background 'tty-menu-disabled-face "gray0")
(set-face-background 'tty-menu-selected-face "gray50")

;; line-number
(set-face-background 'line-number "#2c2c2c")
(set-face-foreground 'line-number-current-line "#859393")

;; trailing-whitespace
(set-face-background 'trailing-whitespace "#2d2d2d")

(provide 'theme-monokai)
;;; theme-monokai.el ends here
