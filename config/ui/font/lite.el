(defun font-config-program ()
  (interactive)
  (set-face-attribute
   'default nil
   :font (font-spec :family "CommitMono" :size 24))
  (dolist (charset '(kana han symbol cjk-misc bopomofo))
    (set-fontset-font
     (frame-parameter nil 'font)
     charset
     (font-spec :family "Sarasa Mono SC"))))

(defun font-config-org-mode ()
  (interactive)
  (set-face-attribute
   'default nil
   :font (font-spec :family "Sarasa Mono SC"))
  (dolist (charset '(kana han symbol cjk-misc bopomofo))
    (set-fontset-font
     (frame-parameter nil 'font)
     charset
     (font-spec :family "Sarasa Mono SC"))))

(if *is-not-a-windows* (font-config-program))

(defun windows-font-set ()
  (set-face-attribute 'default nil :font "CommitMono")
  (dolist (charset '(kana han symbol cjk-misc bopomofo))
    (set-fontset-font
     (frame-parameter nil 'font)
     charset
     (font-spec :name "-outline-微软雅黑-normal-normal-normal-sans-*-*-*-*-p-*-iso10646-1"
                :slant 'normal))))

(if *is-a-windows* (windows-font-set))

;; emoji
(let ((emoji-font-name (if *is-a-mac* "Apple Color Emoji" "Noto Color Emoji")))
  (set-fontset-font "fontset-default" 'unicode emoji-font-name))
(add-to-list 'face-font-rescale-alist '("Apple Color Emoji" . 0.85))

;; minority languages
(set-fontset-font "fontset-default" 'phonetic "Sarasa UI SC")
(set-fontset-font "fontset-default" 'cyrillic "Sarasa UI SC")

(set-fontset-font "fontset-default" 'georgian "Noto Sans Georgian")
(add-to-list 'face-font-rescale-alist '("Noto Sans Georgian" . 0.9))

(set-fontset-font "fontset-default" 'yi "Noto Sans Yi")
(add-to-list 'face-font-rescale-alist '("Noto Sans Yi" . 0.9))

(set-fontset-font "fontset-default" 'canadian-aboriginal "Noto Sans Canadian Aboriginal")
(add-to-list 'face-font-rescale-alist '("Noto Sans Canadian Aboriginal" . 0.9))

(set-fontset-font "fontset-default" 'egyptian "Noto Sans Egyptian Hieroglyphs")
(add-to-list 'face-font-rescale-alist '("Noto Sans Egyptian Hieroglyphs" . 0.8))

(set-fontset-font "fontset-default" 'thai "Noto Sans Thai")
(add-to-list 'face-font-rescale-alist '("Noto Sans Thai" . 0.7))

(set-fontset-font "fontset-default" 'bamum "Noto Sans Bamum")
(add-to-list 'face-font-rescale-alist '("Noto Sans Bamum" . 0.9))

(set-fontset-font "fontset-default" 'syloti-nagri "Noto Sans Syloti Nagri")
(add-to-list 'face-font-rescale-alist '("Noto Sans Syloti Nagri" . 0.9))

(set-fontset-font "fontset-default" 'arabic "DejaVu Sans Mono")
(set-fontset-font "fontset-default" 'arabic "Noto Sans Arabic UI" nil 'append)
(add-to-list 'face-font-rescale-alist '("Noto Sans Arabic UI" . 0.9))

(set-fontset-font "fontset-default" 'armenian "DejaVu Sans Mono")

;; fallback
(dolist (charset '(kana han symbol cjk-misc bopomofo))
  (dolist (fontname '("Jigmo" "Jigmo2" "Jigmo3"))
    (set-fontset-font "fontset-default" charset fontname nil 'append)))

(set-fontset-font t nil "Symbola" nil 'append)
(add-to-list 'face-font-rescale-alist '("Symbola" . 0.85))

(provide 'font/lite)
