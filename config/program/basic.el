;; 格式化整个文件
(defun indent-whole ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil))
  (message "format successfully"))

(global-set-key (kbd "<f12>") 'indent-whole)

;; 格式化光标所在代码段
(global-set-key (kbd "C-c C-\\") (kbd "M-SPC C-M-\\"))

;; 显示光标位置所匹配的括号信息
(leaf mic-paren
  :ensure t
  :config
  (paren-activate))

;; 彩虹括号
(leaf rainbow-delimiters
  :ensure t
  :hook (prog-mode-hook)
  :init
  (show-paren-mode -1))

;; 大小写转换
(leaf string-inflection
  :ensure t
  :bind (("C-c C-u" . string-inflection-ruby-style-cycle)
         ("C-c u" . string-inflection-ruby-style-cycle)))

;; JavaScript js
(setq-default js-indent-level 2)

(leaf yaml-mode
  :ensure t
  :mode ("\\.yml\\'"))

(provide 'program/basic)
