;; GUI 下自动导入 PATH
(leaf exec-path-from-shell
  :ensure t
  :when (memq window-system '(mac ns x))
  :pre-setq
  ((exec-path-from-shell-variables
    .
    '("PATH" "MANPATH" "SSH_AUTH_SOCK" "NIX_SSL_CERT_FILE"))
   ;; start a non-interactive shell (only login shell)
   (exec-path-from-shell-arguments . '("-l")))
  :config
  (exec-path-from-shell-initialize))

;; 终端模式下共享剪贴板
(leaf my-osx-clipboard :when (and *is-a-mac* (not window-system)) :require t)
(leaf my-linux-clipboard :when *is-a-linux* :require t)

;; sudo edit
(leaf auto-sudoedit
  :after (vterm)
  :ensure t
  :setq `((tramp-persistency-file-name . ,(cache-path "tramp")))
  :config
  (auto-sudoedit-mode 1))

(leaf http-server
  :require t
  :config
  (http-server-start-server-after-init))

(provide 'system/advanced)
