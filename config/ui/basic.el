;; 不显示滚动条
(if window-system (scroll-bar-mode 0) (setq scroll-bar-mode nil))
;; 关闭菜单栏
(menu-bar-mode 0)

;; 不自动换行
(set-default 'truncate-lines t)
;; 自动换行但是不截断单词
;; (global-visual-line-mode t)

;; 显示列数
(setq column-number-mode t)

(leaf my-whitespace-mode
  :hook ((prog-mode-hook . local-show-trailing-whitespace)
         (text-mode-hook . local-show-trailing-whitespace)
         (web-mode-hook . local-show-trailing-whitespace)))

(leaf font/lite
  :when window-system
  :require t)

(provide 'ui/basic)
