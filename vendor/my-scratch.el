;;; my-scratch --- my-scratch
;;; Commentary:
;;
;;; Code:
(defun loaded-time-info ()
  "Loaded time info."
  (format "Emacs loaded in %.03fs" (float-time (time-subtract (current-time) before-init-time))))

(defun loaded-info ()
  "Loaded info."
  (concat
   ";; "
   (loaded-time-info)
   " with "
   (number-to-string (length load-path))
   " packages."))

(defvar my-logo-file
  (let ((logo (f-join user-emacs-directory ".cache" "the_warning_label_logo.png")))
    (unless (file-exists-p logo)
      (url-copy-file "http://www.lisperati.com/lisplogo_warning2_256.png" logo))
    logo))

(defun my-scratch--fill-scratch ()
  (setq initial-scratch-message
        (with-temp-buffer
          (if window-system
              (insert-image (create-image my-logo-file) ";; -*- lexical-binding: t -*-")
            (insert ";; -*- lexical-binding: t -*-" "\n")
            (insert ";; WARNING!!! BUILT USING LISP!!!"))
          (insert "\n\n")
          (insert (loaded-info))
          (insert "\n\n")
          (buffer-string))))

(add-hook 'after-init-hook 'my-scratch--fill-scratch)

(provide 'my-scratch)
;;; my-scratch.el ends here
