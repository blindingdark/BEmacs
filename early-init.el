;; 启动时最大化
(add-to-list 'default-frame-alist '(fullscreen . maximized))
;; 不显示工具栏
(tool-bar-mode 0)
;; 不显示欢迎界面
(setq inhibit-startup-message t)
;; lsp-mode
(setenv "LSP_USE_PLISTS" "1")
;; Emacs' default auto-save is stupid to generate #foo# files!
(setq auto-save-default nil
      auto-save-list-file-name nil)
;; package path
(setq package-user-dir (expand-file-name "elpa" (expand-file-name (concat user-emacs-directory ".cache"))))
(startup-redirect-eln-cache (concat (file-name-as-directory ".cache") "eln-cache"))
