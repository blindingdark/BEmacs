;;; package --- Start up
;;; Commentary:
;; 启动配置
;;; Code:
;; 编译 vendor 下的文件
;; (byte-recompile-directory (expand-file-name "vendor" user-emacs-directory) 0)

;; TODO 提取成工具包
(defun buffer-extension-name= (extension &optional file-name)
  "Return not-nil if buffer file extension name or FILE-NAME's extension name equal EXTENSION."
  (string= extension (file-name-extension (or file-name (or buffer-file-name "")))))

(defun add-subdirs-to-load-path (dir)
  "Recursive add directories in DIR to `load-path'."
  (let ((default-directory (file-name-as-directory dir)))
    (add-to-list 'load-path dir)
    (normal-top-level-add-subdirs-to-load-path)))

(defun emacs-base-path (path &optional base)
  "Return absolute PATH BASE 'user-emacs-directory."
  (expand-file-name path (or base user-emacs-directory)))

(defun read-file (path)
  "Return UTF-8 String if PATH exists."
  (ignore-errors
    (with-temp-buffer
      (insert-file-contents-literally (emacs-base-path path))
      (decode-coding-region (point-min) (point-max) 'utf-8 t))))

(defconst *config-path* (emacs-base-path "config"))
(defconst *vendor-path* (emacs-base-path "vendor"))
(defconst *cache-path* (emacs-base-path ".cache"))
(defconst *static-path* (emacs-base-path "static"))

(defun cache-path (path)
  "Return PATH with *cache-path*."
  (emacs-base-path path *cache-path*))

(defun static-path (path)
  "Return PATH with *static-path*."
  (emacs-base-path path *static-path*))

(defun vendor-path (path)
  "Return PATH with *vendor-path*."
  (emacs-base-path path *vendor-path*))

(add-subdirs-to-load-path *config-path*)
(add-to-list 'load-path *vendor-path*)
(setq custom-file (emacs-base-path "custom.el"))

;;; 配置依赖源
(require 'package)
(when (eval-when-compile (version< emacs-version "27"))
  (package-initialize))

(setq package-archives '(("gnu"    . "https://mirrors.sjtug.sjtu.edu.cn/emacs-elpa/gnu/")
                         ("melpa"  . "https://mirrors.sjtug.sjtu.edu.cn/emacs-elpa/melpa/")
                         ("nongnu" . "https://mirrors.sjtug.sjtu.edu.cn/emacs-elpa/nongnu/")))

(defun list-packages-upstream ()
  (interactive)
  (setq package-archives '(("gnu"    . "https://elpa.gnu.org/packages/")
                           ("melpa"  . "https://melpa.org/packages/")
                           ("nongnu" . "https://elpa.nongnu.org/nongnu/")))
  (list-packages))

(unless (package-installed-p 'leaf)
  (unless (assoc 'leaf package-archive-contents)
    (package-refresh-contents))
  (condition-case err
      (package-install 'leaf)
    (error
     (package-refresh-contents)       ; renew local melpa cache if fail
     (package-install 'leaf))))

(leaf leaf-keywords
  :ensure t
  :config (leaf-keywords-init))

;; (leaf benchmark-init
;;   :ensure t
;;   :require t
;;   ;; To disable collection of benchmark data after init is done.
;;   :hook (after-init-hook . benchmark-init/deactivate))

;; 环境常量
(defconst *is-a-linux* (eq system-type 'gnu/linux))
(defconst *is-a-windows* (eq system-type 'windows-nt))
(defconst *is-a-mac* (eq system-type 'darwin))
(defconst *is-not-a-windows* (memq system-type '(darwin gnu/linux)))

(provide 'start)
;;; start.el ends here
