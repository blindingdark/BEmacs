;;; doom --- doom theme
;;; Commentary:
;; 一个很漂亮的暗色系主题
;; 非常的 DOOM 风
;; 非常的压抑
;; 我喜欢警告的 mode-line 闪烁，像是游戏里被击中的闪烁

;;; Code:
(leaf doom-themes
  :ensure t
  :setq ((doom-themes-enable-bold . t)
         (doom-themes-enable-italic . t))
  :preface
  ;; with org-modern
  (declare-function org-delete-all "ext:org-macs" (elts list))
  ;; Corrects (and improves) org-mode's native fontification.
  (advice-add
   'doom-themes-enable-org-fontification :after
   (lambda ()
     (setq
      org-font-lock-extra-keywords
      (org-delete-all
       '(("^ *\\(-----+\\)$" 1 'org-meta-line))
       org-font-lock-extra-keywords))))
  :init
  ;; Doom One
  (load-theme 'doom-one t)
  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)
  (doom-themes-org-config))

;; line-number
(leaf hl-line :require t)
(set-face-background 'line-number "#313335")
(set-face-foreground 'line-number-current-line "#859393")

;; trailing-whitespace
(set-face-background 'trailing-whitespace "#2257A0")

(provide 'theme-doom)
;;; theme-doom.el ends here
