;;; my-surround --- surround 包围
;;; Commentary:
;; 自动配对
;;
;; TODO https://github.com/casouri/isolate
;;
;;; Code:

(global-set-key "\M-'" 'surround-pair)
(global-set-key "\M-\"" 'surround-begin-end)

(defun surround-pair (pair)
  "Surround with char PAIR."
  (interactive "*cPair: ")
  (insert-pair 0 pair pair))

(defun surround-begin-end (begin end)
  "Surround with char BEGIN and END."
  (interactive "*cBegin: \ncEnd: ")
  (insert-pair 0 begin end))

(provide 'my-surround)
;;; my-surround.el ends here
