;;; init.el --- config loader
;;; Commentary:
;;  system/basic 必须加载
;;; Code:

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; start
(load-file (expand-file-name "config/start.el" user-emacs-directory))

;; system
(leaf system/env :require t)
(leaf system/basic :require t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; basic
;; ui
(leaf ui/basic :require t)
;; edit
(leaf edit/basic :require t)
;; program
(leaf program/basic :require t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; advanced
;; system
(leaf system/advanced :require t)
;; ui
(leaf ui/advanced :require t)
;; edit
(leaf edit/advanced :require t)
;; program
(leaf program/advanced :require t)
;; other
(leaf other/advanced :require t)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; custom
(load-file custom-file)

(provide 'init)
;;; init.el ends here
