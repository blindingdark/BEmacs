(leaf telega
  :ensure t
  ;; :load-path "/home/deck/Codes/telega.el/"
  :after (nerd-icons)
  :bind
  ((telega-root-mode-map
    ("M-g s" . telega-saved-messages))
   (telega-chat-mode-map
    ("M-g s" . telega-saved-messages)
    ;; C-return send msg, not return
    ("RET")
    ("<C-return>" . telega-chatbuf-input-send)))
  :setq
  `((telega-use-docker . "podman")
    (telega-docker-run-arguments . "--userns=keep-id")
    ;; 分组前缀
    (telega-chat-folder-format . ,(propertize "%f " 'face 'bold))
    ;; 外部程序播放 gif，这样可以自动循环播放
    (telega-animation-play-inline)
    ;; 关闭这个会让一些花体字显示效果更好
    (telega-emoji-use-images)
    ;; 使用 telegram desktop 的系统通知配置
    (telega-notifications-msg-args
     .
     '(:desktop-entry "telegramdesktop"
                      :urgency "normal"
                      :timeout 5000))
    ;; 消息状态符号
    (telega-symbol-checkmark . ,(nerd-icons-codicon "nf-cod-check"))
    (telega-symbol-heavy-checkmark . ,(nerd-icons-codicon "nf-cod-check_all"))
    (telega-symbol-codeblock . ,(nerd-icons-faicon "nf-fa-code"))
    (telega-sticker-animated-play . t)
    (telega-company-username-show-avatars . t))
  :preface
  ;; style
  ;;;; Thanks @Lucius_Chen
  (defcustom telega-builtin-palettes-alist
    ;; Adjusted palettes for builtin colors with further reduced saturation:
    ;; red, orange, purple/violet, green, cyan, blue, pink
    '((light
       ((:outline "#d32f2f") (:foreground "#c62828")) ; red
       ((:outline "#f57c00") (:foreground "#ef6c00")) ; orange
       ((:outline "#8e24aa") (:foreground "#7b1fa2")) ; purple/violet
       ((:outline "#388e3c") (:foreground "#2e7d32")) ; green
       ((:outline "#0288d1") (:foreground "#0277bd")) ; cyan
       ((:outline "#1976d2") (:foreground "#1565c0")) ; blue
       ((:outline "#d81b60") (:foreground "#c2185b"))) ; pink
      (dark
       ((:outline "#ef9a9a") (:foreground "#e57373")) ; red
       ((:outline "#ffe0b2") (:foreground "#ffcc80")) ; orange
       ((:outline "#ce93d8") (:foreground "#ba68c8")) ; purple/violet
       ((:outline "#a5d6a7") (:foreground "#81c784")) ; green
       ((:outline "#b2ebf2") (:foreground "#80deea")) ; cyan
       ((:outline "#90caf9") (:foreground "#64b5f6")) ; blue
       ((:outline "#f8bbd0") (:foreground "#f48fb1")))) ; pink
    "Alist of builtin colors for light and dark themes, with further reduced saturation for dark theme."
    :package-version '(telega . "0.8.292")
    :type '(alist :key-type
                  (choice (const :tag "Colors for light theme" light)
                          (const :tag "Colors for dark theme" dark))
                  :value-type (repeat (list string)))
    :group 'telega)
  (defface telega-msg-heading
    '((((class color) (background light))
       :background "gray85" :extend t)
      (((class color) (background dark))
       :underline (:color "gray25" :style wave)
       :inherit unspecified
       :background unspecified
       :extend t)
      (t :inherit widget-single-line-field :extend t))
    "Face to display messages header."
    :group 'telega-faces)
  (defface telega-msg-inline-reply
    `((t
       :underline t
       :font ,(font-spec :name "Monaspace Argon") ;; FIXME: 无法响应缩放
       :extend t
       :inherit telega-shadow))
    "Face to highlight replies to messages."
    :group 'telega-faces)
  (defface telega-msg-inline-forward
    `((t
       :underline t
       :font ,(font-spec :name "Monaspace Argon") ;; FIXME: 无法响应缩放
       :extend t
       :inherit unspecified))
    "Face to highlight message forwarding header."
    :group 'telega-faces)
  (defface telega-webpage-chat-link
    '((t
       :background "gray25"
       :extend t
       :inherit unspecified))
    "Face to display `pageBlockChatLink' web page blocks"
    :group 'telega-faces)
  (defun set-telega-root-mode-style ()
    (setcar (alist-get 'dark telega-builtin-palettes-alist)
            `((:outline "#ff0a0a") (:foreground ,(face-attribute 'error :foreground)) (:background "#3d2828")))
    (set-face-foreground 'telega-unmuted-count "#98be65")
    (set-face-background 'telega-webpage-preformatted "gray16")
    (set-face-font 'telega-webpage-fixed (font-spec :name "Sarasa Mono SC"))
    (set-face-font 'telega-entity-type-code (font-spec :name "Sarasa Mono SC"))
    (set-face-font 'telega-entity-type-pre (font-spec :name "Sarasa Mono SC"))
    (setq telega-symbols-emojify (assq-delete-all 'checkmark telega-symbols-emojify))
    (setq telega-symbols-emojify (assq-delete-all 'heavy-checkmark telega-symbols-emojify)))
  (defun set-telega-chat-mode-style ()
    (set-face-attribute 'telega-msg-user-title nil
                        :font (font-spec :family "CommitMono"))
    (set-face-attribute 'telega-box-button nil
                        :height 0.9
                        :box '(:line-width 1 :color "#51afef" :style nil)
                        :background 'unspecified
                        :foreground "#51afef"
                        :inherit 'unspecified)
    (set-face-attribute 'telega-box-button-active nil
                        :height 0.9
                        :box '(:line-width 1 :color "#51afef" :style nil)
                        :background "#285777"
                        :foreground "#51afef"
                        :inherit 'unspecified)
    (set-face-attribute 'telega-entity-type-blockquote nil
                        :underline t
                        :font (font-spec :name "Monaspace Argon")))
  ;; auto complete
  (defun telega-add-company-backends ()
    (set (make-local-variable 'company-backends)
         (append '(telega-company-emoji
                   telega-company-username
                   telega-company-hashtag
                   telega-company-markdown-precode)
                 (when (telega-chat-bot-p telega-chatbuf--chat)
                   '(telega-company-botcmd))))
    (company-mode 1))
  ;; pangu
  (defun telega-pangu (&rest _ignored)
    "Add spacing before send."
    (with-current-buffer (current-buffer)
      (pangu-spacing-search-buffer
       pangu-spacing-include-regexp
       telega-chatbuf--input-marker (point-max)
       (replace-match "\\1 \\2" nil nil))))
  ;; appindicator
  ;; TODO: macos
  (defun telega-appindicator-update! (&rest _ignored)
    "Update appindicator label."
    (when telega-appindicator-mode
      (let* ((uu-chats-num
              (or (plist-get telega--unread-chat-count :unread_unmuted_count)
                  0))
             (mentions-num
              (or (when telega-appindicator-show-mentions
                    (length (telega-filter-chats telega--ordered-chats '(mention))))
                  0))
             (num
              (+ uu-chats-num mentions-num))
             (path
              (if (> num 99)
                  "telega-icons/more.svg"
                (concat "telega-icons/" (number-to-string num) ".svg"))))
        (telega-server--send
         (concat "setup " (static-path path))
         "appindicator"))))
  ;; hooks
  (defun telega-root-mode-hook-fun ()
    ;; highlight
    (hl-line-mode 1)
    ;; style
    (set-telega-root-mode-style))
  (defun telega-chat-mode-hook-fun ()
    ;; company
    (telega-add-company-backends)
    ;; style
    (set-telega-chat-mode-style))
  :config
  ;; fix for pixel scroll mode
  (advice-add 'telega-chatbuf-input-send :after (lambda (&rest _) (recenter -1)))
  ;; pangu
  (advice-add 'telega-chatbuf-input-send :before #'telega-pangu)
  ;; notify
  (telega-notifications-mode 1)
  ;; auto download according to network
  (telega-auto-download-mode 1)
  ;; system tray appindicator
  (if *is-a-linux*
      (advice-add 'telega-appindicator-update :override 'telega-appindicator-update!))
  (telega-appindicator-mode 1)
  :hook ((telega-root-mode-hook . telega-root-mode-hook-fun)
         (telega-chat-mode-hook . telega-chat-mode-hook-fun)))

(leaf telega-bridge-bot :require t :after (telega))

(leaf telega-mnz :require t :after (telega)
  :config
  (add-to-list 'telega-mnz-languages '(elixir . elixir-mode))
  (global-telega-mnz-mode 1))

(leaf telega-url-shorten
  :require t
  :after (telega)
  :setq (telega-url-shorten-use-images . t)
  :config
  (global-telega-url-shorten-mode)
  (add-to-list
   'telega-url-shorten-regexps
   `(too-long-link
     :regexp "^\\(https?://\\)\\(.\\{30\\}\\).*?$"
     :symbol ""
     :replace "\\1\\2...")
   'append))

(leaf org-capture
  :bind (("C-c ESC ESC" . org-capture))
  :preface
  (defun sdcv-translate (word)
    (replace-regexp-in-string
     "--[^>]"
     "\n\n  > "
     (replace-regexp-in-string
      "//"
      " <\n\n"
      (shell-command-to-string
       (format "sdcv -x -n -u \"Longman Dictionary of Contemporary English\" -u \"Oxford Advanced Learner's Dictionary\" %s --data-dir=%s"
               (format "\"%s\"" word)
               "/home/blindingdark/dict")))))
  :setq
  (org-capture-templates
   .
   `(("w"
      "Words"
      entry
      (file+headline "/home/blindingdark/文档/Note/生词本.org" "Words")
      "* %i\n:PROPERTIES:\n:ANKI_DECK: English\n:ANKI_NOTE_TYPE: Basic\n:END:\n** Front\n   %i\n** Back\n   %(sdcv-translate \"%i\")%?"))))

(leaf anki-editor
  :ensure t
  :after (org-capture)
  :preface
  (defun anki-editor--anki-connect-invoke! (orig-fun &rest args)
    (let ((request--curl-callback
           (lambda (proc event) (request--curl-callback "localhost" proc event))))
      (apply orig-fun args)))
  :init
  (advice-add 'anki-editor--anki-connect-invoke :around #'anki-editor--anki-connect-invoke!))

(leaf fanyi
  :ensure t
  :commands fanyi-dwim
  :config
  (require 'fanyi)
  (fanyi-set-providers
   'fanyi-providers
   '(fanyi-longman-provider
     fanyi-haici-provider
     fanyi-etymon-provider
     fanyi-youdao-thesaurus-provider)))

(leaf doctor-chatgpt
  :require t
  :bind
  ((doctor-chatgpt-mode-map
    ("C-c C-r" . azure-tts-play-region)
    ("RET" . nil)
    ("C-<return>" . doctor-chatgpt-ret-or-read))))

(leaf azure-tts
  :require t
  :setq
  `((azure-tts-temp-dir . ,(cache-path "azure-tts"))))

(provide 'other/advanced)
