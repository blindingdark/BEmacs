(let* ((user-env-file (expand-file-name "user-env.el" user-emacs-directory))
       (user-env-file-exists (file-exists-p user-env-file)))
  (if user-env-file-exists
      (load-file user-env-file)))

(provide 'system/env)
