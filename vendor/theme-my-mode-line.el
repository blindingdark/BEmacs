;;; my-mode-line --- custom mode line
;;; Commentary:
;; 自定义 mode line
;; spaceline 太卡，这个比较精简
;; 彩虹猫好可爱的
;; 参考 https://emacs-china.org/t/mode-line/374
;;      https://emacs-china.org/t/topic/655

;;; Code:
(defun mode-line-fill (face reserve)
  "Return empty space using FACE and leaving RESERVE space on the right."
  (unless reserve
    (setq reserve 20))
  (when (and window-system (eq 'right (get-scroll-bar-mode)))
    (setq reserve (- reserve 3)))
  (propertize " "
              'display `((space :align-to
                                (- (+ right right-fringe right-margin) ,reserve)))
              'face face))

(defun mode-icon (icon)
  "Current mode ICON."
  (propertize icon
              'help-echo (format "Major-mode: `%s'" major-mode)
              'display '(raise -0.1)))

(setq-default
 mode-line-format
 (list
  ;; the buffer name; the file name as a tool tip
  " "
  '(:eval (propertize "%b " 'face 'font-lock-keyword-face
                      'help-echo (buffer-file-name)))

  ;; buffer-file-coding-system
  '(:eval (format "[%s] " buffer-file-coding-system))

  ;; relative position, size of file
  "["
  '(:eval (list (nyan-create)))
  ;;(propertize "%p" 'face 'font-lock-constant-face) ;; % above top
  ;;"/"
  ;;(propertize "%I" 'face 'font-lock-constant-face) ;; size
  "] "

  ;; line and column
  "(" ;; '%02' to set to 2 chars at least; prevents flickering
  (propertize "%02l" 'face 'font-lock-type-face) ","
  (propertize "%02c" 'face 'font-lock-type-face)
  ") "

  ;; the current major mode for the buffer.
  "["
  '(:eval (let ((major-mode-icon (nerd-icons-icon-for-mode major-mode))
                (file-name-icon (if buffer-file-name
                                    (nerd-icons-icon-for-file buffer-file-name))))
            (if (symbolp major-mode-icon)
                (if (symbolp file-name-icon)
                    (format-mode-line "%m")
                  (mode-icon file-name-icon))
              (mode-icon major-mode-icon))))
  "] "

  "[" ;; insert vs overwrite mode, input-method in a tooltip
  '(:eval (propertize (if overwrite-mode "Ovr" "Ins")
                      'face 'font-lock-preprocessor-face
                      'help-echo (concat "Buffer is in "
                                         (if overwrite-mode "overwrite" "insert") " mode")))

  ;; was this buffer modified since the last save?
  '(:eval (when (buffer-modified-p)
            (concat ", "  (propertize "Mod"
                                      'face 'font-lock-warning-face
                                      'help-echo "Buffer has been modified"))))

  ;; is this buffer read-only?
  '(:eval (when buffer-read-only
            (concat ", "  (propertize "RO"
                                      'face 'font-lock-type-face
                                      'help-echo "Buffer is read-only"))))
  "] "

  (mode-line-fill nil 11)

  " "
  ;; add the time, with the date and the emacs uptime in the tooltip
  '(:eval (propertize (format-time-string "%H:%M")
                      'help-echo
                      (concat (format-time-string "%c; ")
                              (emacs-uptime "Uptime:%hh"))))

  ;; i don't want to see minor-modes; but if you want, uncomment this:
  ;; minor-mode-alist  ;; list of minor modes
  ))

(leaf nyan-mode
  :ensure t
  :commands nyan-create
  :pre-setq ((nyan-wavy-trail)))

(provide 'theme-my-mode-line)
;;; theme-my-mode-line.el ends here
