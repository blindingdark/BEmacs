(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values '((eval org-content 2)))
 '(warning-suppress-types '((comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ivy-minibuffer-match-face-1 ((t (:inherit font-lock-doc-face :foreground unspecified))) nil "Customized with leaf in `ivy-prescient' block at `/home/blindingdark/.emacs.d/config/edit/advanced.el'")
 '(swiper-background-match-face-1 ((t nil)) nil "Customized with leaf in `swiper' block at `/home/blindingdark/.emacs.d/config/edit/advanced.el'"))
