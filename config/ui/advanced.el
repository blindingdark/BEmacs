;; (leaf theme-dracula :when window-system :require t)
(leaf theme-doom :when window-system :require t)

;;; modeline
(leaf doom-modeline
  :ensure t
  :when window-system
  :preface
  ;; Vterm buffer read-only state by vterm-copy-mode
  (defun doom-modeline-update-buffer-file-state-icon! (orig-fun &rest args)
    (let ((buffer-read-only
           (if (derived-mode-p 'vterm-mode)
               vterm-copy-mode
             buffer-read-only)))
      (apply orig-fun args)))
  (advice-add 'doom-modeline-update-buffer-file-state-icon :around #'doom-modeline-update-buffer-file-state-icon!)
  (defun doom-modeline--modal-icon! (orig-fun text face help-echo &optional icon unicode)
    (if (eq 'doom-modeline-god face)
        (funcall orig-fun text face help-echo "nf-md-gamepad_square" unicode)
      (funcall orig-fun text face help-echo icon unicode)))
  (advice-add 'doom-modeline--modal-icon :around #'doom-modeline--modal-icon!)
  :init
  (doom-modeline-mode))

;; (leaf theme-my-mode-line :when window-system :require t)
;; (leaf theme-spaceline :when window-system :require t) ;; 很酷炫，但是很卡。

(leaf theme-monokai :unless window-system :require t)

(leaf nerd-icons
  :ensure t
  :require t
  :preface
  (defun all-the-icons-faicon (_)
    "for telega-url-shorten.el. we use image so we don't need this function"
    "X")
  (defun all-the-icons-octicon (_ &rest rest)
    "for lsp-modeline--code-actions-icon"
    (nerd-icons-octicon "nf-oct-light_bulb" :face (plist-get rest :face)))
  (defun all-the-icons-material (&rest _)
    "for company-box-icons.el declare-function"
    "X")
  (provide 'all-the-icons)
  :config
  (add-to-list 'nerd-icons-mode-icon-alist
               '(telega-root-mode nerd-icons-faicon "nf-fae-telegram" :face nerd-icons-lblue))
  (add-to-list 'nerd-icons-mode-icon-alist
               '(telega-chat-mode nerd-icons-faicon "nf-fae-telegram" :face nerd-icons-lyellow))
  (add-to-list 'nerd-icons-mode-icon-alist
               '(eaf-mode nerd-icons-octicon "nf-oct-browser" :face nerd-icons-lcyan)))

(leaf nerd-icons-dired
  :ensure t
  :hook (dired-mode-hook))

(leaf f :ensure t :require t)
(leaf s :ensure t :require t)
(leaf my-scratch :require t)

(leaf fira-code-mode :require t)

(pixel-scroll-precision-mode)
(setq pixel-scroll-precision-interpolate-page t)
(defun +pixel-scroll-interpolate-down (&optional lines)
  (interactive)
  (if lines
      (pixel-scroll-precision-interpolate (* -1 lines (pixel-line-height)))
    (pixel-scroll-interpolate-down)))

(defun +pixel-scroll-interpolate-up (&optional lines)
  (interactive)
  (if lines
      (pixel-scroll-precision-interpolate (* lines (pixel-line-height))))
  (pixel-scroll-interpolate-up))

(defalias 'scroll-up-command '+pixel-scroll-interpolate-down)
(defalias 'scroll-down-command '+pixel-scroll-interpolate-up)

;; presentation-mode

(leaf goggles
  "Highlight changes in the buffer."
  :ensure t
  :setq (goggles-pulse . nil))

(leaf beacon
  "Highlight the cursor whenever the window scrolls."
  :ensure t)

(provide 'ui/advanced)
