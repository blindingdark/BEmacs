;;; my-whitespace-mode --- all about whitespace
;;; Commentary:
;; 空白字符相关
;;; Code:

(defun local-show-trailing-whitespace ()
  "Only show at local file."
  (setq-local show-trailing-whitespace t))

(provide 'my-whitespace-mode)
;;; my-whitespace-mode.el ends here
